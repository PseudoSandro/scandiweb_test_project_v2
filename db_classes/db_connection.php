<?php
//Class for connecting to the database, also checking whether a given database exists
//This class does not have a namespace, because for some reason the PDO class (MYSQLi also, i tried it to no avail)
//Was not accessible when under a namespace
class DbConnect
{
    private $pdo;
    private $servername = "localhost";
    private $username = "root";
    private $dbname = "products";
    private $tablename = "LIST";

    //function for connecting to DB using PDO
    public function connect()
    {
        try {
            $this->pdo = new PDO("mysql:host=$this->servername", $this->username);
            return $this->pdo;
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }

    //Function that creates the database 'products' if it is not already created
    public function createDB()
    {
        try {
            $sql = "CREATE DATABASE $this->dbname";
            $this->pdo->exec($sql);
        } catch (PDOException $e) {
            $e->getMessage();
        }
    }

    //Fetches data from the DB
    public function getData()
    {
        $data = $this->pdo->query("SELECT * FROM $this->dbname.$this->tablename ORDER BY date DESC")->fetchAll();
        return $data;
    }

    //This function checks if there is already a table with the given name in the DB
    //If there is not, then creates it
    public function checkTable()
    {
        try {
            $this->pdo->query("SELECT 1 FROM $this->dbname.$this->tablename");
            return;
        } catch (PDOException $e) {
            $sql2 = "CREATE TABLE $this->dbname.$this->tablename(
                sku varchar(100) PRIMARY KEY NOT NULL,
                name varchar(100) NOT NULL,
                price DECIMAL(10, 2) NOT NULL,
                size INT,
                weight INT,
                height INT,
                width INT,
                length INT,
                type varchar(100) NOT NULL,
                date TIMESTAMP
            )";
            $this->pdo->exec($sql2);
        }
    }

    //Method for deleting products from the server
    //First argument is the length of the DELETE array, or how many items there is to delete
    //The second argument is the POST object that contains the SKUs of products to be deleted
    //The third argument is the PDO connection object

    public function delete($count, $post_object, $pdo_object)
    {
        //This loops through the POST object and sends DELETE queries for corresponding SKUs
        for ($i = 0; $i < $count; $i++) {
            $sku = strval($post_object['delete'][$i]);
        
            $sql = "DELETE FROM $this->dbname.$this->tablename WHERE sku = '$sku'";
            $pdo_object->exec($sql);
        }
    }

    //method for sending product info to the server, it accepts the PDO connection object and also
    //an object containing info that a user has typed into the form
    public function add($pdo_object, $product_object)
    {
        $sql = "INSERT INTO $this->dbname.$this->tablename(sku, 
        name, 
        price, 
        size, 
        weight, 
        height, 
        width, 
        length, 
        type, 
        date) 
        VALUES('$product_object->sku', 
        '$product_object->name', 
        $product_object->price, 
        $product_object->size, 
        $product_object->weight, 
        $product_object->height, 
        $product_object->width, 
        $product_object->length, 
        '$product_object->type', 
        CURRENT_TIMESTAMP)";

        $pdo_object->exec($sql);
    }
}
