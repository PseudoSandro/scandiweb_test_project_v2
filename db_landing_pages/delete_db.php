<?php
// Product deletion handling page

header("refresh:2;url='../index.php'");

//Sets up PDO
include_once("../db_classes/db_connection.php");
$connection = new DbConnect();
$pdo = $connection->connect();

//Loop through the SKUs and send DELETE queries for each of them to the DB
$connection->delete(count($_POST['delete']), $_POST, $pdo);

echo "Deleting your products...";
