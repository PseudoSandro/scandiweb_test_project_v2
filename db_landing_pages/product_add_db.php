<?php
header("refresh:2;url='../index.php'");
include("../db_classes/db_connection.php");
include('../product_classes/product_class.php');
include('../product_classes/book_service.php');
include('../product_classes/dvd_service.php');
include('../product_classes/furniture_service.php');
//Connects to the DB
$connection = new DbConnect();
$pdo = $connection->connect();

//Create an appropriate class for the given product type
$product_class = Products::getClass($_POST);

//Set appropriate attributes to be sent to the server
$product_class->set();

//Send data to server
$connection->add($pdo, $product_class);

echo "Adding your Product...";
