<?php
//The main page that displays Products that we have saved to the Database

require("product_classes/product_class.php");
require("db_classes/db_connection.php");
require("product_classes/dvd_service.php");
require("product_classes/book_service.php");
require("product_classes/furniture_service.php");

//Connects to the DB
//Also, checks whether the DB or table are already created, if not, creates them
$connection = new DbConnect();
$connection->connect();
$connection->createDB();
$connection->checkTable();

//Fetches Data from the DB
$productList = $connection->getData();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Product List</title>
    <script
  src="https://code.jquery.com/jquery-3.5.1.min.js"
  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
  crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <style>
        <?php include 'index.css'; ?>
    </style>
</head>
<body>
    <div class="container-fluid">
        <header id="headerContainer">
            <h1>Product List</h1>
            <nav id="addButton">
                <a href="product_add/product_add.html">
                    <button class="btn btn-primary">Add New</button>
                </a>
                <button class ="btn btn-danger" type="submit" form="deleteForm" disabled=true>DELETE SELECTED</button>
            </nav>
        </header>
        <hr />
        <!-- The form tag allows the checkboxes to be posted to the deletion page-->
        <form method="post" action="db_landing_pages/delete_db.php" id="deleteForm">
            <div class="row">
        <?php

        //The common attributes of products are handled using the common displayCommon method that is inherited
        //from the base product class

        //the getClass methods gets info about a given products type, returns the corresponding class and
        //it is handled using the displayUnique method of the corresponding class (yay polymorphism)

        //Also, this checks if there is any info to display (the IF clause)
        if (count($productList) > 0) {
            foreach ($productList as $listItem) {
                $product = Products::getClass($listItem);
                $product->displayCommon($listItem);
                $product->displayUnique($listItem);
            }
        }
        ?>
        </div>
    </div>
    <script>
        //This Script Enables and Disables the DELETE button, depending on whether at least
        //One checkbox is checked
        $("input").click(() => {
            if($("input:checked").length == 0) {
                $(".btn-danger").attr("disabled", true)
                return;
            }
            $(".btn-danger").attr("disabled", false)
        });
    </script>
</body>
</form>
</html>