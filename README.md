# Scandiweb Test Project


# Hello!
This is my first project with PHP, SQL and using No Frameworks (Since my primary experience is with React) - learning these technologies was a fun challenge and i hope you'll like this project.

## Technologies Used:

- PHP (installed using XAMPP)
- I used the PSR-2 Standard
- MariaDB (It came with XAMPP, and as i searched around, i read that it's practically the same as MySQL)
- Bootstrap
- A lil bit of Jquery
- CodeSniffer library, installed with composer

## Installation

- I developed this project using the latest version of XAMPP, so for the app to function, you have to save the root folder to the 'htdocs' folder in XAMPP, so the "htdocs" folder has to be the localhost root.
- The username for the DB has to be 'root', no password.
- The DB name is 'products', it's on the default 3306 port.

## Additional Note

- I also added a third page, that displays to the user the product info he/she is about to send to the server, thought it would be useful.