<?php
require_once('product_class.php');

    //This class is made for displaying the particular product type
class DvdService extends Products
{
        //Displays Products on the "Product List" Page
    public function displayUnique($item)
    {
        echo $item['size'] . " MB" . "<br \>";
        echo "</div>";
    }

    //Sets the unique attribute that's to be sent to the server
    public function set()
    {
        $this->size = $this->trimmer(intval($_POST['size']));
    }
}
