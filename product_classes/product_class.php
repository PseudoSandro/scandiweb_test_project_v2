<?php

//The Base Product class that handles all common product logic
class Products
{
    public $type;
    public $sku;
    public $name;
    public $price;
    public $size = 0;
    public $weight = 0;
    public $height = 0;
    public $width = 0;
    public $length = 0;

        //The constructor sets all common properties from the POST global object, after clicking the form
    public function __construct()
    {
        if (isset($_POST['type'])) {
            $this->type = strval($_POST["type"]);
            $this->sku = strval($_POST["sku"]);
            $this->name = strval($_POST["name"]);
            $this->price = floatval($_POST["price"]);
        }
    }

        //This function grabs the 'type' property from the supplied object and creates the
        //matching class, to be used for sending the product data to server
    public static function getClass($object)
    {
        $class = new $object['type'];
        return $class;
    }

        //This function is used in index.php for displaying the common properties of the product types
    public function displayCommon($item)
    {
        $sku = $item["sku"];

        echo "<div class='item col-lg-2 col-sm-4' id=$sku>";
        echo "<input id='check' type='checkbox' name='delete[]' value='$sku'>";
        echo "<em>" . $item['sku'] . "</em>" . "<br \>";
        echo "<strong>" .$item['name'] . "</strong>" ."<br \>";
        echo $item['price'] . "$" ."<br \>";
    }

        //Some data formatting
    public function trimmer($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
}
