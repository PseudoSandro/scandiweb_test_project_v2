<?php

require_once('product_class.php');

    //This class is made for displaying the particular product type
class BookService extends Products
{
        //Displays Products on the "Product List" Page
    public function displayUnique($item)
    {
        echo $item['weight'] . " KG" . "<br \>";
        echo "</div>";
    }

    //Sets the unique attribute that's to be sent to the server
    public function set()
    {
        $this->weight = $this->trimmer(intval($_POST['weight']));
    }
}
