//Handles product type switching, adds relevant fields & HTML validation using ReGex
$("#types").click(() => {
    let value = $("#types").val();
    switch (value) {
        case "DvdService":
            $(".optionalAttribute").html(`<table>
                                            <tr>
                                                <td>
                                                    <label for="size">Size</label>
                                                </td>
                                                <td>
                                                    <input required type = "text" name = "size" id = "size" pattern="\\d+">
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-info btn-sm"data-toggle="tooltip"
                                                        data-placement="right" title="Size: size of the DVD in Megabytes, must be a whole number">
                                                        ?
                                                    </button>
                                                </td>
                                            </tr>
                                        </table>`)
            //Enables Bootstrap Tooltips
            $('[data-toggle="tooltip"]').tooltip()
            break;

        case "BookService":
            $(".optionalAttribute").html(`<table>
                                            <tr>
                                                <td>
                                                    <label for="weight">Weight</label>
                                                </td>
                                                <td>
                                                    <input required type="text" name="weight" id="weight" pattern="\\d+">
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-info btn-sm" data-toggle="tooltip"
                                                        data-placement="right" title="Weight: Weight of the Book in Kilograms, must be a whole number">
                                                        ?
                                                    </button>
                                                </td>
                                            </tr>
                                        </table>`)
            //Enables Bootstrap Tooltips
            $('[data-toggle="tooltip"]').tooltip()
            break;

        case "FurnitureService":
            $('[data-toggle="tooltip"]').tooltip()
            $(".optionalAttribute").html(`<table>
                                            <tr>
                                                <td>
                                                    <label for="height">Height</label>
                                                </td>
                                                <td>
                                                    <input required type="text" name="height" id="height" pattern="\\d+"> 
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-info btn-sm" data-toggle="tooltip"
                                                        data-placement="right" title="Height: Height in Milimeters, must be a whole number">
                                                        ?
                                                    </button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label for="width">Width</label>
                                                </td>
                                                <td>
                                                    <input required type="text" name="width" id="width" pattern="\\d+">
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-info btn-sm" data-toggle="tooltip"
                                                        data-placement="right" title="Width: Width in Milimeters, must be a whole number">
                                                        ?
                                                    </button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label for="length">Length</label>
                                                </td>
                                                <td>
                                                    <input required type="text" name="length" id="length" pattern="\\d+">
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-info btn-sm" data-toggle="tooltip"
                                                        data-placement="right" title="Length: Length in Milimeters, must be a whole number">
                                                    ?
                                                    </button>
                                                </td>
                                            </tr>
                                        </table>`)
            //Enables Bootstrap Tooltips
            $('[data-toggle="tooltip"]').tooltip()
            break;
    }
});

//Reset button for the form
$("#clear").click(() => {
    $("#mainForm").trigger("reset");
})